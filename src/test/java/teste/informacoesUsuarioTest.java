package teste;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class informacoesUsuarioTest {

    //declarando um atributo/variavel do tipo WebDriver navegador na classe informacoesusuarioTest
    public WebDriver navegador;

    //setando tudo que todos os testes precisam fazer antes de iniciar com o before
    @Before
    public void setUp() {
        //setando o webdriver
        WebDriverManager.chromedriver().setup();
        //criando um objeto new ChromeDriver
        navegador = new ChromeDriver();
        //maximizando a janela do chrome
        navegador.manage().window().maximize();
        //pedindo para o navegador esperar um tempo de 5 segundos para buscar os elementos na tela
        navegador.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
        //navegando até o site
        navegador.get("http://www.juliodelima.com.br/taskit/");
        //clicar no link de texto Sign in
        navegador.findElement(By.linkText("Sign in")).click();
        //identificando formulario id signinbox e colocando ele em uma variavel
        //pois o usarei mais de uma vez
        WebElement formularioSignInBox = navegador.findElement(By.id("signinbox"));

        //clicar no campo com name login que esta dentro do formulario de id signinbox e digitar FelipeMuginsk
        formularioSignInBox.findElement(By.name("login")).sendKeys("muginski");

        //clicar no campo com name password que esta dentro do formulario de id signinbox e digitar abc123
        formularioSignInBox.findElement(By.name("password")).sendKeys("123456");

        //clicar no link com o texto SIGN IN
        formularioSignInBox.findElement(By.linkText("SIGN IN")).click();
    }
    @Test
    public void testLoginComSucesso() {

        //validar dentro do elemento com class "me" o texto "Hi, Felipe Muginsk"
        WebElement me = navegador.findElement(By.className("me"));

        //pegando o texto que tem dentro da classe me
        String textoNoElementoMe = me.getText();

        // garantindo que o texto na variavel textoNoElementoMe é igual o texto apresentado no site
        Assert.assertEquals(textoNoElementoMe,"Hi, Felipe Muginsk");

    }
    @Test
    public void testAdicionarUmTelefoneNoUsuario() {

        //clicar em um um link que possui a classe "me"
        navegador.findElement(By.className("me")).click();

        //clicar no elemento pelo seu xpath //*[@href="#moredata"]
        navegador.findElement(By.xpath("//*[@href=\"#moredata\"]")).click();

        //clicar no elemento pelo seu xpath //button[@data-target="addmoredata"]
        navegador.findElement(By.xpath("//button[@data-target=\"addmoredata\"]")).click();

        //identificar a popup onde esta o  formulario de id addmoredata
        WebElement popUp = navegador.findElement(By.id("addmoredata"));

        //na combo de name "type" escolher a opção "Phone". elementos que sao selects, precisam ser identificados e
        //armazenados em uma outra variavel no caso abaixo "opcao" entao vc da um new selec passando essa variavel e
        //escolhe se quer setar pelo index ou pelo texto visivel que é o que o usuario ve
        WebElement opcao = popUp.findElement(By.name("type"));
        new Select(opcao).selectByVisibleText("Phone");

        //no campo de name "contact" digitar "+551177777777"
        popUp.findElement(By.name("contact")).sendKeys("+5511111111111");

        //clicar no texto de um link "SAVE" SEMPRE QUE FOR LINK TEXTO É O QUE O USUARIO VE
        popUp.findElement(By.linkText("SAVE")).click();

        //na mensagem de id "toast-container" validar que o texto é "Your contact has been added!"
        WebElement textoDoLink = navegador.findElement(By.id("toast-container"));
        //necessario criar uma variavel do tipo string nesse caso mensagem para pegar o texto do elemento
        //que foi identificado acima "toast-container" mas foi atribuido na varivel textoDolink
        String mensagem = textoDoLink.getText();

        Assert.assertEquals("Your contact has been added!", mensagem );

    }

    @Test

    public void testDeletarUmTelefoneDoUsuario(){

        //clicar em um um link que possui a classe "me"
        navegador.findElement(By.className("me")).click();

        //clicar no elemento pelo seu xpath //*[@href="#moredata"]
        navegador.findElement(By.xpath("//*[@href=\"#moredata\"]")).click();

        //clicar no elemento pelo seu xpath //span[text()="+5511997312361"]/following-sibling::a
        navegador.findElement(By.xpath("//span[text()=\"+5511997312361\"]/following-sibling::a")).click();

        //Confirmar a janela de java script
        navegador.switchTo().alert().accept();

        //validar que a mensagem apresentada foi "Rest in peace, dear phone"
        WebElement textoDoLink = navegador.findElement(By.id("toast-container"));
        String mensagem = textoDoLink.getText();
        Assert.assertEquals("Rest in peace, dear phone!", mensagem);

        //aguardar até 10 segundos ate que a janela desapareça
        WebDriverWait aguardar = new WebDriverWait( navegador, Duration.ofSeconds(10));
        aguardar.until(ExpectedConditions.stalenessOf(textoDoLink));

        //fazer logout
        navegador.findElement(By.linkText("Logout")).click();

    }


    @After
    public void tearDown(){
        //fechando navegador
        //navegador.quit();
    }
}

