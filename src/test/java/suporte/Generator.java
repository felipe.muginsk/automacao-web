package suporte;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class Generator {

    public static String dataHoraParaArquivo () {
        //tipo de varivel timestamp e criei ela passando o parado para o sistema, pegar a correnta data millis
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        //criei um retorno usando o metodo que retorna o formato da data e mandei colocar esse formato no ts
        return new SimpleDateFormat( "yyyyMMddhhmmss").format(ts);

    }
}
